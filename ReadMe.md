# Map Explorer

This is repo is for Binance quiz using Go Lang

## The Problem

![quiz](quiz.jpeg "Quiz description")

## The Solution

### Run

From command line "Terminal", go to the source folder and execute the binary file. For quick view, run `./map-explorer`

The program accepts command line args:

- `-minesThreshold=NUM` where "NUM" represents the difficulty. (Default is 21)
- `-useAnimation=true` to draw each step on the screen, however it'll work only when minesThreshold is less than 11 because greater threshold requires larger maps that will not fit on screen. (Default is false)

For quick run with animation `./map-explorer -minesThreshold=9 -useAnimation=true`

### Test

To run the tests, you'll need GO installed and the source folder is placed in the correct $GOPATH `src` directory

Tests can be run using `go test -v`
