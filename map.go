package main

import (
	"fmt"
	"math"
	"strconv"
)

// Map represents the layout mapping coordinates
type Map struct {
	mapp           map[int]map[int]int
	minesThreshold int
	maxAxeYLength  int
}

// NewMap constructor
func NewMap(_minesThreshold int) *Map {
	m := &Map{minesThreshold: _minesThreshold, mapp: map[int]map[int]int{}}
	m.initMap()
	return m
}

func (m *Map) initMap() {
	m.maxAxeYLength = m.calcMaxYValue(m.minesThreshold)
}

func (m *Map) calcPoint(p int) int {
	point := math.Abs(float64(p))
	sum := 0.0
	for point != 0 {
		sum += math.Mod(point, 10)
		point = math.Floor(point / 10)
	}
	return int(sum)
}

func (m *Map) calcMaxYValue(minesThreshold int) int {
	for i := 0; ; i++ {
		if m.calcPoint(i) > minesThreshold {
			return i
		}
	}
}

func (m *Map) isValidLocation(location Location) bool {
	return (m.calcPoint(location.x) + m.calcPoint(location.y)) <= m.minesThreshold
}

func (m *Map) visitLocation(location Location) int {
	if m.mapp[location.x] == nil {
		m.mapp[location.x] = map[int]int{}
	}
	m.mapp[location.x][location.y] = m.mapp[location.x][location.y] + 1
	return m.mapp[location.x][location.y]
}

func (m *Map) isVisitedLocation(location Location) int {
	if m.mapp[location.x] == nil {
		m.mapp[location.x] = map[int]int{}
	}
	return m.mapp[location.x][location.y]
}

func (m *Map) inititalizeEmptyMap(maxSize int) [][]int {
	grid := make([][]int, maxSize)
	for i := 0; i < maxSize; i++ {
		grid[i] = make([]int, maxSize)
		for y := 0; y < maxSize; y++ {
			grid[i][y] = 0
		}
	}

	return grid
}

func (m *Map) mapXYToGridRepresentation() [][]int {
	maxSize := (m.maxAxeYLength * 2) + 1

	grid := m.inititalizeEmptyMap(maxSize)

	for x := range m.mapp {
		for y := range m.mapp[x] {
			grid[y+m.maxAxeYLength][x+m.maxAxeYLength] = m.mapp[x][y]
		}
	}

	return grid
}

func (m *Map) printGrid(grid [][]int) {
	fmt.Println("--------------------------------")

	row := ""
	accessibleLocations := 0

	for x := len(grid) - 1; x >= 0; x-- {
		row = ""
		for y := len(grid[x]) - 1; y >= 0; y-- {
			if grid[x][y] > 0 {
				row += strconv.Itoa((grid[x][y])) + "  "
				accessibleLocations++
			} else {
				row += ".  "
			}
		}
		fmt.Println(row)
	}

	fmt.Println("--------------------------------")
	fmt.Println("Accessible Locations: ", accessibleLocations)
	fmt.Println("--------------------------------")
}

func (m *Map) printMap() {
	m.printGrid(m.mapXYToGridRepresentation())
}

func (m *Map) calcAccessibleLocations() int {
	count := 0
	for x := range m.mapp {
		for y := range m.mapp[x] {
			if m.mapp[x][y] > 0 {
				count++
			}
		}
	}
	return count
}
