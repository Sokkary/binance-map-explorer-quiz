package main

import (
	"flag"
	"fmt"
	"time"
)

var minesThreshold int
var useAnimation bool

func init() {
	flag.IntVar(&minesThreshold, "minesThreshold", 21, "specifies how mines are distributed accross the map.")
	flag.BoolVar(&useAnimation, "useAnimation", false, "Determines if the map is to be drawn on every new step. Can be used with a max minesThreshold of 10 otherwise the drawing will not fit on the screen. (Default false)")
	flag.Parse()
}

func draw(m *Map) {
	if useAnimation && minesThreshold < 11 {
		time.Sleep(10 * time.Millisecond)
		m.printMap()
	}
}

func explore(p *Person, m *Map) int {
	// TODO: find better optimized exit codition
	for revisitsFrequency := 0; revisitsFrequency < minesThreshold*2; {

		if m.isValidLocation(p.nextMoveLocation()) {

			if m.isValidLocation(p.rightLocation()) && m.isVisitedLocation(p.rightLocation()) < m.isVisitedLocation(p.leftLocation()) && (m.isVisitedLocation(p.rightLocation()) < m.isVisitedLocation(p.nextMoveLocation())) {
				p.turnRight()
			} else if m.isValidLocation(p.leftLocation()) && m.isVisitedLocation(p.leftLocation()) < m.isVisitedLocation(p.nextMoveLocation()) {
				p.turnLeft()
			}

			revisitsFrequency = m.visitLocation(p.moveForward(true))

			draw(m)

			continue

		}

		if m.isValidLocation(p.rightLocation()) && m.isVisitedLocation(p.rightLocation()) < m.isVisitedLocation(p.leftLocation()) {
			p.turnRight()
			continue
		}

		if m.isValidLocation(p.leftLocation()) {
			p.turnLeft()
			continue
		}

		p.turn180()

	}

	return m.calcAccessibleLocations()
}

func main() {

	fmt.Printf("Calculating accessible locations for minesThreshold(%d)... \n", minesThreshold)

	m := NewMap(minesThreshold)
	p := NewPerson(0, 0)

	accessibleLocations := explore(p, m)

	fmt.Printf("Using minesThreshold (%d), will result in (%d) accessible locations.\n", minesThreshold, accessibleLocations)
}
