package main

import "testing"

func TestCalcPoint(t *testing.T) {
	m := NewMap(5)
	total := m.calcPoint(123)
	if total != 6 {
		t.Errorf("calcPoint(123) was incorrect, got: %d, want: %d.", total, 6)
	}

	total = m.calcPoint(-123)
	if total != 6 {
		t.Errorf("calcPoint(-123) was incorrect, got: %d, want: %d.", total, 6)
	}

	total = m.calcPoint(102)
	if total != 3 {
		t.Errorf("calcPoint(102) was incorrect, got: %d, want: %d.", total, 3)
	}
}

func TestIsValidLocation(t *testing.T) {
	m := NewMap(5)
	isValid := m.isValidLocation(Location{1, 5})
	if isValid {
		t.Errorf("isValidLocation(1,5) was incorrect, got: %t, want: %t.", isValid, !isValid)
	}

	isValid = m.isValidLocation(Location{0, 5})
	if !isValid {
		t.Errorf("isValidLocation(0,5) was incorrect, got: %t, want: %t.", !isValid, isValid)
	}

	isValid = m.isValidLocation(Location{10, 10})
	if !isValid {
		t.Errorf("isValidLocation(10,10) was incorrect, got: %t, want: %t.", !isValid, isValid)
	}

}
