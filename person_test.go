package main

import "testing"

func TestMoveForward(t *testing.T) {
	p := NewPerson(0, 0)

	location := p.moveForward(true)
	if location.x != 0 || location.y != 1 {
		t.Errorf("p.moveForward(true) was incorrect, got: %d,%d, want: %d,%d.", location.x, location.y, 0, 1)
	}
}

func TestTurnRight(t *testing.T) {
	p := NewPerson(0, 0)

	p.turnRight()
	location := p.moveForward(true)
	if location.x != 1 || location.y != 0 {
		t.Errorf("p.turnRight() was incorrect, got: %d,%d, want: %d,%d.", location.x, location.y, 1, 0)
	}

	p.turnRight()
	location = p.moveForward(true)
	if location.x != 1 || location.y != -1 {
		t.Errorf("p.turnRight() was incorrect, got: %d,%d, want: %d,%d.", location.x, location.y, 1, -1)
	}
}
