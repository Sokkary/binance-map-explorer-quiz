package main

const (
	_Up    int = 1
	_Right int = 2
	_Down  int = 3
	_Left  int = 4
)

// Location represents x,y co-ordinates for a location point
type Location struct {
	x int
	y int
}

// Person properties and methods
type Person struct {
	location  Location
	direction int
}

// NewPerson constructor
func NewPerson(x, y int) *Person {
	return &Person{Location{x, y}, _Up}
}

// Move forward/backward depedning on the "step" param +/-
func (p *Person) move(step int) Location {
	x := p.location.x
	y := p.location.y

	if p.direction == _Up {
		y += step
	}

	if p.direction == _Right {
		x += step
	}

	if p.direction == _Left {
		x += (-1 * step)
	}

	if p.direction == _Down {
		y += (-1 * step)
	}

	return Location{x, y}
}

func (p *Person) moveForward(isActualMove bool) Location {
	if isActualMove {
		p.location = p.move(1)
		return p.location
	}

	return p.move(1)
}

func (p *Person) moveBack(isActualMove bool) Location {
	if isActualMove {
		p.location = p.move(-1)
		return p.location
	}

	return p.move(-1)
}

func (p *Person) turnRight() {
	p.direction++
	if p.direction > 4 {
		p.direction = 1
	}
}

func (p *Person) turnLeft() {
	p.direction--
	if p.direction < 1 {
		p.direction = 4
	}
}

func (p *Person) nextMoveLocation() Location {
	return p.moveForward(false)
}

func (p *Person) currentLocation() Location {
	return p.location
}

func (p *Person) leftLocation() Location {
	x := p.location.x
	y := p.location.y

	if p.direction == _Up {
		x--
	}

	if p.direction == _Right {
		y++
	}

	if p.direction == _Left {
		y--
	}

	if p.direction == _Down {
		x++
	}

	return Location{x, y}
}

func (p *Person) rightLocation() Location {
	x := p.location.x
	y := p.location.y

	if p.direction == _Up {
		x++
	}

	if p.direction == _Right {
		y--
	}

	if p.direction == _Left {
		y++
	}

	if p.direction == _Down {
		x--
	}

	return Location{x, y}
}

func (p *Person) backLocation() Location {
	x := p.location.x
	y := p.location.y

	if p.direction == _Up {
		y--
	}

	if p.direction == _Down {
		y++
	}

	if p.direction == _Right {
		x--
	}

	if p.direction == _Left {
		x++
	}

	return Location{x, y}
}

func (p *Person) turn180() {
	p.turnRight()
	p.turnRight()
}
