package main

import "testing"

func TestExplore5(t *testing.T) {
	m := NewMap(5)
	p := NewPerson(0, 0)

	accessibleLocations := explore(p, m)
	if accessibleLocations != 61 {
		t.Errorf("explore(5) was incorrect, got: %d, want: %d.", accessibleLocations, 61)
	}
}

func TestExplore10(t *testing.T) {
	m := NewMap(10)
	p := NewPerson(0, 0)

	accessibleLocations := explore(p, m)
	if accessibleLocations != 1121 {
		t.Errorf("explore(10) was incorrect, got: %d, want: %d.", accessibleLocations, 1121)
	}
}

func TestExplore21(t *testing.T) {
	m := NewMap(21)
	p := NewPerson(0, 0)

	accessibleLocations := explore(p, m)
	if accessibleLocations != 287881 {
		t.Errorf("explore(21) was incorrect, got: %d, want: %d.", accessibleLocations, 287881)
	}
}
